service:
  name: udagram-vj
# app and org for use with dashboard.serverless.com
#app: your-app-name
#org: your-org-name


# Add the serverless-webpack plugin
plugins:
  - serverless-webpack
  - serverless-reqvalidator-plugin
  - serverless-aws-documentation

provider:
  name: aws
  runtime: nodejs12.x
  region: ${opt:region, 'us-east-2'}
  stage: ${opt:stage, 'dev'}

  environment:
    GROUPS_TABLE: Groups-${self:provider.stage}
    IMAGES_TABLE: Images-${self:provider.stage}
    IMAGE_ID_INDEX: ImageIdIndex-${self:provider.stage}

  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:Scan
        - dynamodb:PutItem
        - dynamodb:GetItem
      Resource: arn:aws:dynamodb:${self:provider.region}:*:table/${self:provider.environment.GROUPS_TABLE}
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:PutItem
      Resource: arn:aws:dynamodb:${self:provider.region}:*:table/${self:provider.environment.IMAGES_TABLE}

    - Effect: Allow
      Action:
        - dynamodb:Query
      Resource: arn:aws:dynamodb:${self:provider.region}:*:table/${self:provider.environment.IMAGES_TABLE}/index/${self:provider.environment.IMAGE_ID_INDEX}


custom:
  documentation:
    api:
      info:
        version: v1.0.0
        title: Udagram API vj
        description: Serverless application for images sharing
    models:
      - name: GroupRequest
        contentType: application/json
        schema: ${file(models/create-group-request.json)}
      - name: ImageRequest
        contentType: application/json
        schema: ${file(models/create-image-request.json)}

functions:
  GetGroups:
    handler: src/lambda/http/getGroups.handler
    events:
      - http:
          method: get
          path: groups
          cors: true
  GetImages:
    handler: src/lambda/http/getImages.handler
    events:
      - http:
          method: get
          path: groups/{groupId}/images
          cors: true
  GetImage:
    handler: src/lambda/http/getImage.handler
    events:
      - http:
          method: get
          path: images/{imageId}
          cors: true

  CreateGroup:
    handler: src/lambda/http/createGroup.handler
    events:
      - http:
          method: post
          path: groups
          cors: true
          reqValidatorName: RequestBodyValidator
          documentation:
            summary: Create a new Group
            description: Create a new Group
            requestModels:
              'application/json': GroupRequest

  CreateImage:
    handler: src/lambda/http/createImage.handler
    events:
      - http:
          method: post
          path: groups/{groupId}/images
          cors: true
          reqValidatorName: RequestBodyValidator
          documentation:
            summary: Create a new Image into group
            description: Create a new Image into group
            requestModels:
              'application/json': ImageRequest


resources:
  Resources:
    RequestBodyValidator:
      Type: AWS::ApiGateway::RequestValidator
      Properties:
        Name: 'request-body-validator'
        RestApiId:
          Ref: ApiGatewayRestApi
        ValidateRequestBody: true
        ValidateRequestParameters: false

    GroupsDynamoDBTable:
      Type: AWS::DynamoDB::Table
      Properties:
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
              - AttributeName: id
                KeyType: HASH
        BillingMode: PAY_PER_REQUEST
        TableName: ${self:provider.environment.GROUPS_TABLE}

    ImagesDynamoDBTable:
      Type: AWS::DynamoDB::Table
      Properties:
        AttributeDefinitions:
          - AttributeName: groupId
            AttributeType: S
          - AttributeName: timestamp
            AttributeType: S
          - AttributeName: imageId
            AttributeType: S
        KeySchema:
              - AttributeName: groupId
                KeyType: HASH
              - AttributeName: timestamp
                KeyType: RANGE
        GlobalSecondaryIndexes:
          - IndexName: ${self:provider.environment.IMAGE_ID_INDEX}
            KeySchema:
            - AttributeName: imageId
              KeyType: HASH
            Projection:
              ProjectionType: ALL

        BillingMode: PAY_PER_REQUEST
        TableName: ${self:provider.environment.IMAGES_TABLE}
